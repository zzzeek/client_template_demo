"""The single view.

The view here returns a simple, JSON-compatible Python data structure.

The Pyramid @view_config decorators define four different methods 
of how the server can interpret the delivery of this data structure.

1. server_navigate - renders the "home.mako" page on the server, 
   which as part of its server-side rendering routes the data 
   into the display_api() Mako def for display.

2. server_ajax - this renders the API data table on the server,
   via the display_api() def present in the "home.mako" Mako page,
   then delivers the complete HTML snippet.
   
3. client_navigate - renders the "display_api.handlebars" client-side
   page, by first serving the server-side "handlebars_base.mako" template,
   which once rendered on the client, initiates the javascript to 
   display the client side page, interpreting the data which is also
   delivered via the server side render.   
   
   Any fully client-rendering application needs some server-side 
   rendering step like this in order to deliver the  "base" HTML 
   to a web browser.
   
4. api_ajax - this is the "pure API" call, returning the data
   directly as JSON text, to be interpreted by a pure client
   side call.

"""
from pyramid.view import view_config
import random

@view_config(route_name='server_navigate', renderer='home.mako')
@view_config(route_name='server_ajax', renderer='home|display_api.mako')
@view_config(route_name='client_navigate', renderer='display_api.handlebars')
@view_config(route_name='api_ajax', renderer='json')
def api(request):
    return [
        ["%0.2d" % random.randint(1, 99)  for col in xrange(10)]
        for row in xrange(10)
    ]
