<!DOCTYPE html>
<html lang="en">
    <head>
        <title>
            <%block name="title">
                Client / Server / API Template Demo
            </%block>
        </title>
        <%block name="head">
        <link rel="stylesheet" href="${request.static_url('template_demo:static/css/style.css')}"/>
        <script src="${request.static_url('template_demo:static/js/jquery-1.7.1.min.js')}"></script>
        <script src="${request.static_url('template_demo:static/js/handlebars-1.0.0.beta.6.js')}"></script>
        <script src="${request.static_url('template_demo:static/js/app.js')}"></script>
        <script>
            var static_url = "${request.static_url('template_demo:static/')}";
        </script>

        </%block>
    </head>
    <body>
        <div id="container">
            <div id="nav">
                <ul>
                    <li><a href="${request.route_url('server_navigate')}">Server via Server</a></li>
                    <li><a href="#" onclick="jQuery('#content').load('${request.route_url('server_ajax')}')">Server via Client</a></li>
                    <li><a href="${request.route_url('client_navigate')}">Client via Server</a></li>
                    <li><a href="${request.route_url('client_navigate')}"
                            api="${request.route_url('api_ajax')}" 
                            template="display_api.handlebars">Client via Client</a></li>
                </ul>
            </div>

        <div id="content">
            ${next.body(**pageargs)}
        </div>

        </div>
    </body>
</html>