<%inherit file="layout.mako"/>

<%block name="head">
    ${parent.head()}
    <script>
        $(document).ready(function(){
            app.render(${data}, "${handlebars}", "#content", true);
        });
    </script>
</%block>

