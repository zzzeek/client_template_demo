var app = function(window, $, hb) { 
    var _template_cache = {};

    /**
     * get_template
     *
     * Retrieve a handlebars template from the
     * _template_cache collection.  If not present, 
     * retrieve it from the server's
     * static/templates directory, compile it, and store the
     * compiled form in the _template_cache collection,
     * returning the compiled form.  
     */
    function get_template(tname) {
        if (_template_cache[tname] == undefined) {
            $.ajax(static_url + "templates/" + tname, {
                type: "GET",
                success: function(data, textStatus, jqXHR) {
                    _template_cache[tname] = hb.compile(data);
                },
                async:false,
                dataType: 'text',
                cache: true,
                error: function(jqXHR, textStatus, exception) {
                    alert("Error loading template '" + 
                            tname + "'; " + 
                            textStatus + " " + 
                            exception);
                }
            });
        }
        return _template_cache[tname];
    }

    /**
     * render
     *
     * Render a handlebars template given the 
     * template name, data for render, and DOM
     * target.
     *
     * The is_page flag indicates the source of the
     * render for demonstration purposes.
     * 
     */
    function render(data, tname, target, is_page) {
        if (target == undefined) {
            target = "#content";
        }
        var template = get_template(tname);
        var html = template({data:data, is_page:is_page});
        $(target).html(html);
    }
    
    /**
     * render_via_api
     *
     * Respond to a link which contains api href and
     * template name.  Call the server's api href via 
     * AJAX, assigning as a callback a call to the 
     * render() function which will render the given template
     * client-side.
     */
    function render_via_api(api, tname) {
        $.ajax(api,{
            success:function(data, textStatus, jqXHR) {
                render(data, tname, undefined, false);
            },
            error: function(jqXHR, textStatus, exception) {
                alert('Error: ' + textStatus + ' ' + exception);
            }
        })
    }


    $(document).ready(function() {
        $("body a[api]").unbind('click').click(function() {
            var a = $(this);
            render_via_api(a.attr('api'), a.attr('template'));
            return false;
        });
    });
    
    /* the public api of the app object. */
    return {
        render : render
    }
}(window, jQuery, Handlebars);