from pyramid.asset import abspath_from_asset_spec
from mako.lookup import TemplateLookup
import re

class MakoRenderer(object):
    def __init__(self, info):
        package_name = info.package.__name__
        self.lookup = TemplateLookup(
                        directories=abspath_from_asset_spec(
                                "%s:templates" % package_name
                        ))
        self.template_reg = re.compile(
                    r'(?P<fname>[\w_]+)'
                    r'(?:\|(?P<defname>[\w_]+))?'
                    r'(\.(?P<extension>.*))')

    def __call__(self, value, system):
        return self._render(
                    system['renderer_name'], 
                    data=value, 
                    request=system['request'])

    def _render(self, tname, **kw):
        fname, defname, ext = self.template_reg.match(
                                        tname
                                    ).group(
                                        "fname", "defname", "extension"
                                    )
        template = self.lookup.get_template("%s.%s" % (fname, ext))
        if defname:
            template = template.get_def(defname)
        return template.render_unicode(**kw)

class HandlebarsRenderer(MakoRenderer):
    def __call__(self, value, system):
        return super(HandlebarsRenderer, self)._render(
                "handlebars_base.mako", 
                data=value, 
                request=system['request'],
                handlebars=system['renderer_name'])

