from pyramid.config import Configurator
from .renderers import MakoRenderer
from .renderers import HandlebarsRenderer

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_renderer('.mako', MakoRenderer)
    config.add_renderer('.handlebars', HandlebarsRenderer)

    config.add_route('server_navigate', '/')
    config.add_route('client_navigate', '/client_navigate')
    config.add_route('server_ajax', '/server_ajax')
    config.add_route('api_ajax', '/api_ajax')

    config.scan()
    return config.make_wsgi_app()

